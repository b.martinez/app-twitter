const express = require('express');
const router = express.Router();
const dbset = require('../dbConfig/dataSet');

router.get('/', function (req, res, next) {
    res.render('register');
});

router.post('/', function (req, res) {
    const request = req.body;
    if (dbset.dbManager.isDBCreated()) {
        console.log('problema');
        findUser();
    }
    else {
        res.render('register', { message: "Error de conexión" });
    }

    function findUser() {
        dbset.UserManager.findUser(request.username, function (err, user) {
            console.log(err);
            if (err) { console.log('error'); res.render('register', err); return; }
            if (user === null) {
                console.log('null user');
                dbset.UserManager.insertUser({
                    id: null,
                    username: '@' + request.username,
                    password: request.password,
                    following: [],
                    followers: []
                }, function (error, data) {
                    if (error) {
                        res.render('register', error);
                    }
                    else {
                        res.redirect('/success');
                    }
                })
            }
            else {
                console.log('else');
                res.render('register', { message: "Usuario existente" });
            }
        });
    }
});

module.exports = router;
