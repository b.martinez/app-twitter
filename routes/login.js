const express = require('express');
const dbset = require('../dbConfig/dataSet');
const router = express.Router();


router.get('/', function (req, res, next) {
  if (req.session.user !== undefined) {
    res.redirect('/searchUsers')
  }
  else {
    res.render('login');
  }
});

router.post('/', function (req, res) {
  const request = req.body;
  const dataUser = { username: request.username, password: request.password };

  dbset.UserManager.verifyUser(dataUser, function (err, user) {
    console.log(err);
    let message = err ? err.message : 'Usuario o password incorrectos';
    if (user !== null) {
      req.session.user = user;
      res.redirect('/searchUsers');
    }
    else {

      res.render('login', { "message": message });
    }
  });
});

router.get('/logout', function (req, res) {
  req.session.destroy(function () { });
  res.redirect('/');
});

module.exports = router;
