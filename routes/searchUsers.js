const express = require('express');
const router = express.Router();
const dbset = require('../dbConfig/dataSet');
const follow = require('../dbConfig/follow');
/* GET home page. */
router.get('/', function (req, res) {
    if (req.session.user === undefined) {
        res.redirect('/');
    }
    else {
        const sessData = req.session;
        res.render('searchUsers', { "username": sessData.user.username });
    }
});

router.post('/', function (req, res, next) {
    const request = req.body;
    const sessData = req.session.user;
    let isFollowing = false;
    dbset.UserManager.findUser(request.username, function (err, user) {
        let message = err ? err.message : "El usuario " + request.username + " no se encuentra registrado";
        if (user === null || err) {
            res.render('searchUsers', { "username": sessData.username, "message": message });
        }
        else {
            isFollowing = follow.isFollowing(sessData, request);
            res.render('searchUsers', { "datauser": user.username, "username": sessData.username, "following": user.following, "isFollowing": isFollowing });
        }
    });
});

router.get('/follow', function (req, res, next) {
    res.redirect('/searchUsers');
});

router.post('/follow', function (req, res, next) {
    const request = req.body;
    const sessData = req.session.user;
    follow.followUser(sessData, request.username, function (err, currentUser, followUser) {
        if (err) {
            res.render('searchUsers', { "datauser": request.username, "username": sessData.username, "following": followUser.following, "messageError": err.message });
        }
        else {
            req.session.user = currentUser;
            res.render('searchUsers', { "datauser": request.username, "username": sessData.username, "following": followUser.following, "isFollowing": true, "messageAdd": "Agregado" });
        }
    });
});

module.exports = router;
