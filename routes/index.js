const express = require('express');
const router = express.Router();

router.get('/', function (req, res, next) {
    res.redirect('/login')
});

router.get('/success', function (req, res, next) {
    res.render('pageSuccess')
});

module.exports = router;