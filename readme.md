App Twitter
Ejercicio basado en la funcionalidad básica de twitter
=====================================

## Pre-requisitos

Tener instalado nodejs
https://nodejs.org/es/download/

## Instalación de paquetes ##
Situarse en el directorio raiz del proyecto y ejecutar el comando:
-> npm install

## Levantar la aplicación ##
Situarse en el directorio raiz del proyecto y ejecutar el comando:
-> npm start

## Ruta de acceso por defecto ##
http://localhost:3000/