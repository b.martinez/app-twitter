const fs = require("fs");
const env = require('./env');
const messageErrorDB = { message: "Error de conexión" };

function isDBCreated() {
    let fileExists = fs.existsSync(env.dbPath);
    if (!fileExists) { return CreateDB(); }
    return true;
}

function CreateDB() {
    try {
        fs.mkdirSync(env.dirPath);
        fs.writeFileSync(env.dbPath, '[]');
        return true;
    } catch (error) {
        return false;
    }
}

function readData(callback) {
    fs.readFile(env.dbPath, function (err, data) {
        if (err) { return callback(messageErrorDB, null); };
        const usersData = JSON.parse(data.toString());
        callback(null, usersData);
    });
}


module.exports = { isDBCreated, readData, messageErrorDB }