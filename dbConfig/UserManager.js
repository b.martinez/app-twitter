const dbManager = require("./dbManager");
const fs = require("fs");
const env = require('./env');

function formatUserName(username) {
    return username.indexOf('@') === -1 ? '@' + username : username;
}

function findUser(username, callback) {
    username = formatUserName(username);
    dbManager.readData(function (err, data) {
        if (err) { callback(dbManager.messageErrorDB, null); return; }
        for (let i = 0; i < data.length; i++) {
            if (data[i].username === username) {
                data[i].password = null;
                callback(err, data[i]);
                return;
            }
        }
        callback(err, null);
    });

}

function insertUser(user, callback) {
    dbManager.readData(function (err, data) {
        if (err) { callback(dbManager.messageErrorDB, null); return; }
        data.push(user);
        fs.writeFile(env.dbPath, JSON.stringify(data), (err) => {
            if (err) { return callback(dbManager.messageErrorDB, null); }
            callback(null, user);
        });
    });
}

function verifyUser(dataUser, callback) {
    dataUser.username = formatUserName(dataUser.username);
    dbManager.readData(function (err, data) {
        if (err) { return callback(dbManager.messageErrorDB, null); }
        for (let i = 0; i < data.length; i++) {
            if (data[i].username === dataUser.username && data[i].password == dataUser.password) {
                data[i].password = null;
                callback(null, data[i]);
                return;
            }
        }
        callback(null, null);
    });
}

module.exports = { formatUserName, findUser, insertUser, verifyUser }