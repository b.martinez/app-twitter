const dbset = require('../dbConfig/dataSet');
const fs = require("fs");
const env = require('./env');

function followUser(currentUser, followTo, callback) {

    let _currentUser = null;
    let followUser = null;

    dbset.dbManager.readData(function (err, data) {
        if (err) { return callback(err, currentUser, followUser); }

        for (let index = 0; index < data.length; index++) {
            const user = data[index];
            if (user.username === currentUser.username) {
                _currentUser = user;
            }
            if (user.username === followTo) {
                followUser = user;
            }
            if (_currentUser !== null && followUser !== null) {
                break;
            }
        }
        if (_currentUser !== null && followUser !== null) {
            _currentUser.following.push(followUser.username);
            updateUser(data, _currentUser, function (err, updatedUser) {
                if (err) { return callback(err, updatedUser, followUser); }
                callback(null, updatedUser, followUser);
                return true;
            });
        }
    });

}

function isFollowing(currentUser, toFollowUser) {
    toFollowUser.username = dbset.UserManager.formatUserName(toFollowUser.username);
    if (currentUser.following.lastIndexOf(toFollowUser.username) >= 0 || currentUser.username === toFollowUser.username) { return true }
    return false;
}

function updateUser(data, user, callback) {
    try {
        const index = data.indexOf(user);
        data[index] = user;
        fs.writeFile(env.dbPath, JSON.stringify(data), (err) => {
            if (err) { return callback(dbset.dbManager.messageErrorDB, user); }
            user.password = null;
            callback(null, user);
        });

    } catch (error) {
        callback(dbset.dbManager.messageErrorDB, null);
    }
}


module.exports = { followUser, isFollowing };